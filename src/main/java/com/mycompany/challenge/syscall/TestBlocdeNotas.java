/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.challenge.syscall;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 *
 * @author docente
 */
public class TestBlocdeNotas {
    
    public static void main(String[] args) throws FileNotFoundException {
        String ruta="src/main/java/datos/respuesta.txt";
        Scanner sc=new Scanner(System.in);
        System.out.println("Digite cadena a guardar en archivo:");
        String cadena=sc.nextLine();
        
        //Cambiando la salida standar
        PrintStream out = new PrintStream(new FileOutputStream(ruta));
        System.setOut(out);
        System.out.println(cadena);
        
        SysCall syscall = new SysCall();
        String[] command = {
                "notepad", ruta
            };
        syscall.run(command);
        
    }
}
