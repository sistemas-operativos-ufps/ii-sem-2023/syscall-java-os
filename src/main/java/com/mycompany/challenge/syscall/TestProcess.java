/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.challenge.syscall;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to test the challenge of listing processes by memory usage
 * @author sebastianperez
 */
public class TestProcess {

    public static void main(String[] args) {
        //Change the method depending on the operating system 
        //on which you are running the program, by default this example was 
        //running in MacOS system
        System.out.println("Review the contents of the resulting file");
        
        try {
            testFromMacOS();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testFromMacOS() throws FileNotFoundException{
        SysCall syscall = new SysCall();
        
        String[] command = {
                "bash",
                "-c",
                "ps aux | awk '{sub(\".*/\", \"\", $11); if ($11 in mem) mem[$11] += $6; else mem[$11] = $6} END {for (p in mem) printf \"%s | %s\\n\", p, (mem[p] \" KB\")}' | sort -t'|' -k2,2nr"
            };
        
        //Set standard output to file with name "result"
        PrintStream out = new PrintStream(new FileOutputStream("src/main/java/com/mycompany/challenge/syscall/result-mac.txt"));
        System.setOut(out);
        
        System.out.println("PROCESS NAME        USAGE RAM");
        System.out.println("===============|================");
        
        syscall.run(command);
        syscall.printer();
    }
    
    public static void testFromWindows() throws FileNotFoundException{
        SysCall syscall = new SysCall();
        
        String[] command = {
                "powershell",
                "-Command",
                "Get-Process | ForEach-Object { $mem[$_.ProcessName] += $_.WorkingSet -as [int] }" +
                " -End { foreach ($p in $mem.Keys) { Write-Host $p, ($mem[$p] / 1MB) } } | Sort-Object { $_.Split(',')[1] -as [double] } -Descending"
            };
        
        //Set standard output to file with name "result"
        PrintStream out = new PrintStream(new FileOutputStream("src/main/java/com/mycompany/challenge/syscall/result-windows.txt"));
        System.setOut(out);
        
        System.out.println("PROCESS NAME        USAGE RAM");
        System.out.println("===============|================");
        
        syscall.run(command);
        syscall.printer();
    }
    
}
