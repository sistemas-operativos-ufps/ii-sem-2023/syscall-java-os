
# Challenge by using SYSCALL with Java language

**Repository solution to the challenge of listing processes in descending order according to memory usage**

The challenge has been tested using a MacOS system, it generates the result of the command in a file called **result-mac.txt** located in the same directory as the program code.